# Gitter account

A Gitter account is associated with GitLab/GitHub/Twitter and matches whatever info you have on that platform.

If you updated some info on GitLab/GitHub/Twitter, sign out of Gitter and sign back in to have it updated.


## Can I merge/connect my accounts?

There isn't a way to merge accounts.


## Can I change my username?

You can't change your username. Your username matches whatever OAuth provider you signed in with.

If you changed your username on GitHub, sign out of Gitter and sign back in again to update it.


### How do I remove the  `_gitlab`/`_twitter` suffix from my username

See above, you can't change your username.

We add the `_gitlab`/`_twitter` suffix to avoid name collisions with GitHub.
If you don't want the suffix added, sign in with GitHub.

You can track [#1851](https://gitlab.com/gitlab-org/gitter/webapp/issues/1851)
for the ability to customize your username in the future and remove the suffix.

## How do I update my avatar?

Sign out of Gitter and sign back in to update your avatar (or any other info).


## How do I delete my account?

Whilst we're very sad to see you go, we're more than happy to help you out. Please contact support@gitter.im with your Gitter username and we'll take care of that for you.

We'd love to hear why so we can improve our service going forward.
