#!/bin/bash
set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function startRedis {
  cp ./config/sentinel.dev.conf.template ./config/sentinel.dev.conf

  redis-server --loglevel warning --port 6379 --bind 127.0.0.1 &
}

function startMongo {
  rm -f /usr/local/var/log/mongodb/mongo.log

  ulimit -n 1000
  mkdir -p /usr/local/var/mongodb/r0
  mkdir -p /usr/local/var/mongodb/r1

  mongod run \
    --config "$SCRIPT_DIR/config/mongo-server.one.yml" \
    --profile=1 \
    --slowms=15 &

  mongod run \
    --config "$SCRIPT_DIR/config/mongo-server.two.yml" \
    --profile=1 \
    --slowms=15 &

  while [[ "$(mongo --quiet --eval 'db.serverStatus().ok') || echo 0" = "0" ]]; do
    echo Waiting for replicaset to come online
    sleep 1
  done

  if [ "$(mongo --quiet --eval 'rs.status().ok')" -eq "0" ]; then
  	echo Replicaset not initialised. Initialising
  	./scripts/mongo/init-dev-mongo.sh

  	while [[ "$(mongo --quiet --eval 'rs.status().ok')" = "0" ]]; do
  		echo Waiting for replicaset to come online
  		mongo --eval 'printjson(rs.status())';
  		sleep 1
  	done

  	./scripts/upgrade-data.sh
  fi
}

(startRedis 1>&2 > /dev/null) &
(startMongo 1>&2 > /dev/null) &
